### Codavel SDK ###

The purpose of this project is to demonstrate how easy is to integrate Codavel SDK into an Android app, by modifying Easy_xkcd, a beautiful, fast, and easy to use xkcd reader for Android.

You can check the original README [here](README.original.md), or directly in the original repository [here](https://github.com/tom-anders/Easy_xkcd).


### Integration ###

In order to integrate Codavel SDK, the following modifications to the original project were required (you can check all the code changes [here](https://gitlab.com/codavel/Easy_xkcd/-/compare/master...codavel?from_project_id=29758839)):

1. Added maven repository and Codavel's Application ID and Secret to the app's build.gradle, required to download and use Codavel's SDK.
2. Start Codavel's Service when the main activity is created, so that our SDK can start processing the HTTP requests. 
3. Register our HTTP interceptor into the multiple app's OkHTTPClient, so that all the HTTP requests executed through the app are processed and forwarded to our SDK.
4. Modify the app's Glide module to use an OkHTTPClient registered with our HTTP interceptor, so that all HTTP requests executed by Glide are also processed by our SDK.
